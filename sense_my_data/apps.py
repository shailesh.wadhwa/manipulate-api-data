from django.apps import AppConfig


class SenseMyDataConfig(AppConfig):
    name = 'sense_my_data'
