import json
import urllib
import pandas as pd
from pandas.io.json import json_normalize

class FetchData():

	url = "http://0.0.0.0:5000/api/pi"
	
	def get_sensor_data(self):
		response = urllib.request.urlopen(self.url)
		jsonResp = response.readall().decode('utf-8')
		jsondata = json.loads(jsonResp)
		df_input = json_normalize(jsondata)
		return df_input
