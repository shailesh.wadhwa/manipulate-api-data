from django.db import models

# Create your models here.
class MockData(models.Model):
	cpu_temp = models.CharField(max_length=200)
	timestamp = models.CharField(max_length=200)
	fqdn = models.CharField(max_length=200)
	corrected_temp = models.CharField(max_length=200)
	location = models.CharField(max_length=200)
	temperature = models.CharField(max_length=200)
	engineers = models.CharField(max_length=200)
	project = models.CharField(max_length=200)
	latitude = models.CharField(max_length=200)
	pressure = models.CharField(max_length=200)
	longitude = models.CharField(max_length=200)
	uuid = models.CharField(max_length=200)
	sensorid = models.CharField(max_length=200)
	humidity = models.CharField(max_length=200)
	sealevel = models.CharField(max_length=200)
	pi_ver = models.CharField(max_length=200)


	def __str__(self):
		return str(self.id)
