from django.shortcuts import render

from django.http import HttpResponse
from sense_my_data.models import MockData

from .db_populate import PopulateDB
from sense_my_data.fetch_data import FetchData



def index(request):
	records = MockData.objects.order_by('id')
	context = {"records": records}
	db_object = PopulateDB()
	fetch_data_obj = FetchData()
	if(request.method == 'POST'):
		df_input = fetch_data_obj.get_sensor_data()
		mockObject = MockData(corrected_temp = df_input["corrected_temp"][0], humidity = df_input["humidity"][0], timestamp = df_input["timestamp"][0])
		mockObject.save()
		context["ParameterY"] = list(MockData.objects.values_list('corrected_temp', flat=True))
		context['timestampX'] = list(MockData.objects.values_list('timestamp', flat = True))
	return render(request, 'sense_my_data/index.html', context)
	
		





# Create your views here.
